#+TITLE: Dotfiles på isidor
#+AUTHOR: Stoffe

* Tmux
** ~/.config/tmux/tmux.conf
:PROPERTIES:
:header-args: :tangle "~/.config/tmux/tmux.conf" :mkdirp yes :comments link
:ID:       557bf3ac-5a3a-4eef-87b2-982b3d7f85e0
:END:

#+begin_src conf :comments no
##########################
# Tangled with org-babel #
##########################
#+end_src

*Terminfo for True Color*

#+begin_src conf
set -g default-terminal "tmux-direct"
#set -as terminal-features ",gnome*:RGB"
#set -as terminal-features ",konsole*:RGB"
set -as terminal-features ",foot*:RGB"
#+end_src

*Scrollback*

#+begin_src conf
set -g history-limit 100000
#+end_src

*Terminalfönster*

#+begin_src conf
# Start window indexing at one instead of zero
set -g base-index 1

# Enable window titles
set -g set-titles on

# Window title string (uses statusbar variables)
set -g set-titles-string '#T'

# don't rename windows automatically
set-option -g allow-rename off
#+end_src

*Ladda om config*

#+begin_src conf
# Bind to reload config
bind r source-file ~/.config/tmux/tmux.conf
#+end_src

*Emacs tangentbordsgenvägar. C-t passar bättre som prefix med Emacs*

#+begin_src conf
# Emacs!
set-option -g status-keys emacs

# Change prefix to C-t unbind C-b
set-option -g prefix C-t
#+end_src

*Visual bell*

#+begin_src conf
# don't do anything when a 'bell' rings
set -g visual-activity off
set -g visual-bell off
set -g visual-silence off
setw -g monitor-activity off
set -g bell-action none
#+end_src

*Kopiera*

#+begin_src conf
# copy mode
setw -g mode-style 'fg=colour0 bg=colour229 bold'
#+end_src

*Statusbar*

#+begin_src conf
set -g status on
set -g status-interval 15
set -g status-justify left
set -g status-interval 15
set -g status-justify left
set -g status-left ''
set -g status-left-length 40
set -g status-left-style default
set -g status-position bottom
set -g status-right "#[fg=colour229 bold,bg=default,bright][#{=-100:pane_title}]"
set -g status-right-length 100
set -g status-right-style default
#+end_src

*Färger*

#+begin_src conf
set -g status-bg "#352718"
set -g status-fg "#e8e4b1"
setw -g window-status-current-style 'fg=colour00 bg=colour2 bold'
setw -g window-status-current-format ' #I #W #F '
setw -g window-status-style 'fg=colour229 bold'
setw -g window-status-format ' #I #[fg=colour229]#W #[fg=colour229]#F '
setw -g window-status-bell-style 'fg=colour229 bg=colour1 bold'
set -g message-style 'fg=colour229 bold bg=colour0 bold'
#+end_src

* Systemd
** ~/.config/systemd/user/tmux.service
:PROPERTIES:
:header-args: :tangle "~/.config/systemd/user/tmux.service" :mkdirp yes :comments link
:ID:       ddaedddf-4b8d-47bd-953d-60673f03c175
:END:

#+begin_src systemd :comments no
##########################
# Tangled with org-babel #
##########################
#+end_src

#+begin_src systemd
[Unit]
Description=tmux session for user %u

[Service]
Type=forking
ExecStart=/usr/bin/tmux new-session -s %u -d
ExecStop=/usr/bin/tmux kill-session -t %u
WorkingDirectory=~

[Install]
WantedBy=default.target
#+end_src

** ~/.config/systemd/user/loopia-dns.service
:PROPERTIES:
:header-args: :tangle "~/.config/systemd/user/loopia-dns.service" :mkdirp yes :comments link
:ID:       cb5617fe-ac3a-4bef-8da8-98ca7b666dcc
:END:

#+begin_src systemd :comments no
##########################
# Tangled with org-babel #
##########################
#+end_src

*Service för uppdatering av IP hos Loopia*

#+begin_src systemd
[Unit]
Description=Uppdatera DNS hos Loopia
After=network.target

[Service]
Type=oneshot
WorkingDirectory=%h
ExecStart=%h/.local/bin/dyndns_loopia-sec.py
ExecStart=%h/.local/bin/dyndns_loopia-www.py

[Install]
WantedBy=default.target
#+end_src

** ~/.config/systemd/user/loopia-dns.timer
:PROPERTIES:
:header-args: :tangle "~/.config/systemd/user/loopia-dns.timer" :mkdirp yes :comments link
:ID:       e4489c2f-f6d2-44a1-85ae-9647c1a02a1c
:END:

*Kör var femte minut*

#+begin_src systemd :comments no
##########################
# Tangled with org-babel #
##########################
#+end_src

#+begin_src systemd
[Unit]
Description=Uppdatera Loopia DNS var femte minut

[Timer]
OnBootSec=3min
OnUnitActiveSec=5min

[Install]
WantedBy=timers.target
#+end_src
