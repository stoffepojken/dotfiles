#+TITLE: Lokala ebuilds på isidor
#+AUTHOR: Stoffe

*Glöm inte att göra manifest med "pkgdev manifest"*

* kodi-pvr-mythtv
** /var/db/repos/local/media-plugins/kodi-pvr-mythtv/kodi-pvr-mythtv-21.1.11.ebuild
:PROPERTIES:
:header-args: :tangle "/var/db/repos/local/media-plugins/kodi-pvr-mythtv/kodi-pvr-mythtv-21.1.11.ebuild" :mkdirp yes :padline no :comments no
:ID:       02103527-2e99-42a7-8c5b-2aa19e0b2615
:END:

#+begin_src conf
# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake kodi-addon

DESCRIPTION="MythTV PVR for Kodi"
HOMEPAGE="https://github.com/janbar/pvr.mythtv"
SRC_URI=""

case ${PV} in
9999)
        SRC_URI=""
        EGIT_REPO_URI="https://github.com/janbar/pvr.mythtv.git"
        inherit git-r3
        ;;
*)
        CODENAME="Omega"
        KEYWORDS="~amd64"
        SRC_URI="https://github.com/janbar/pvr.mythtv/archive/${PV}-${CODENAME}.tar.gz -> ${P}.tar.gz"
        S="${WORKDIR}/pvr.mythtv-${PV}-${CODENAME}"
        ;;
esac

LICENSE="GPL-2"
SLOT="0"
IUSE=""

DEPEND="
        sys-libs/zlib
        media-tv/kodi
        "
RDEPEND="
        ${DEPEND}
        "
#+end_src
