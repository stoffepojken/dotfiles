#+TITLE: Systemkonfiguration på kurre
#+AUTHOR: Stoffe

* System
**  /etc/dracut.conf.d/99-options.conf
:PROPERTIES:
:header-args: :tangle "/etc/dracut.conf.d/99-options.conf" :comments link :mkdirp yes
:ID:       a7cf5ed1-8831-49b4-85a0-c55b01eb7540
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Inställningar för initramfs*

#+begin_src conf
kernel_cmdline="root=PARTUUID=7da9fef6-f938-459a-9350-d0a14143d79d rootflags=defaults,noatime rootfstype=ext4 resume=PARTUUID=62c20846-414f-46b5-975c-f6d2dfd32a30 quiet systemd.show_status=1"
uefi="yes"
hostonly="yes"
early_microcode="yes"
compress="zstd"
add_drivers+=" i915 "
omit_dracutmodules+=" busybox "
add_dracutmodules+=" i18n systemd resume "
filesystems+=" vfat ext4 "
#+end_src

** /etc/fstab
:PROPERTIES:
:header-args: :tangle "/etc/fstab" :comments link :mkdirp yes
:ID:       69763fa9-af11-4fef-acba-14578baa2b1f
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src conf
PARTUUID=f458867c-3de0-4274-b8d3-8acdd1f68305   /efi        vfat    umask=0077		0 2
PARTUUID=62c20846-414f-46b5-975c-f6d2dfd32a30   none        swap    sw			0 0
PARTUUID=7da9fef6-f938-459a-9350-d0a14143d79d   /           ext4    defaults,noatime        0 1
#+end_src

**  /etc/systemd/zram-generator.conf.d/zram0-portage-tmpdir.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/zram-generator.conf.d/zram0-portage-tmpdir.conf" :comments link :mkdirp yes
:ID:       68f4b199-472d-4e63-9094-a81269bbeb12
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*/var/tmp/portage  i  zram*

#+begin_src conf
[zram0]
# use compressed RAM (zram) as portage TMPDIR /var/tmp/portage
# (make sure TMPDIR in /etc/make.conf matches the mount-point below)

## SIZE
# use 1/2 of physical RAM (e.g. on a system with 64 GB RAM: ~32 GB)
zram-size = ram / 3
# use 3/4 of physical RAM (e.g. 64 GB: ~48 GB)
#zram-size = ram / 4 * 3
# use 7/8 of physical RAM (e.g. 64 GB: ~56 GB)
#zram-size = ram / 8 * 7
# fixed size:
#zram-size = 32G

## COMPRESSION
# fast: lzo
# faster: lz4
# best: zstd
compression-algorithm = zstd

## MOUNT-POINT TMPDIR
mount-point = /var/tmp/portage

## MOUNT OPTIONS
#fs-type = ext2
fs-type = ext2
options = rw,nosuid,nodev,discard,X-mount.owner=portage,X-mount.group=portage,X-mount.mode=775
#+end_src

**  /etc/systemd/zram-generator.conf.d/zram1-tmp.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/zram-generator.conf.d/zram1-tmp.conf" :comments link :mkdirp yes
:ID:       ad065b7c-87b4-4304-98c4-ffeda4525d0b
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*/tmp i zram*

#+begin_src conf
[zram1]
# use compressed RAM (zram) as system /tmp

## SIZE
# use 1/16 of physical RAM (e.g. on a system with 64 GB RAM: ~4 GB)
zram-size = ram / 4
# fixed size:
#zram-size = 4096m

## COMPRESSION
# fast: lzo
# faster: lz4
# best: zstd
compression-algorithm = zstd

## MOUNT-POINT
mount-point = /tmp

## MOUNT OPTIONS
#fs-type = ext2
fs-type = ext2
options = rw,nosuid,nodev,discard,X-mount.mode=1777
#+end_src

**  /etc/systemd/system/getty@tty1.service.d/noclear.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/system/getty@tty1.service.d/noclear.conf" :comments link :mkdirp yes
:ID:       b341b8a7-946f-4264-a472-88fcd1c0ef66
:END:

#+begin_src systemd :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Snippet  för att inte rensa TTY-meddelanden*

#+begin_src systemd
[Service]
TTYVTDisallocate=no
#+end_src

* Språk och tangentbord
** /etc/locale.gen
:PROPERTIES:
:header-args: :tangle "/etc/locale.gen" :comments link :mkdirp yes
:ID:       8d06a3a1-17f9-4ffc-b30a-d2db862ac3c7
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src conf
en_US.UTF-8 UTF-8
sv_SE.UTF-8 UTF-8
#+end_src

** /etc/locale.conf
:PROPERTIES:
:header-args: :tangle "/etc/locale.conf" :comments link :mkdirp yes
:ID:       e38c4176-fd84-461e-8406-3f3ff1e0574f
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src conf
LANG=en_US.UTF-8
LC_CTYPE=sv_SE.UTF-8
LC_NUMERIC=sv_SE.UTF-8
LC_TIME=sv_SE.UTF-8
LC_MONETARY=sv_SE.UTF-8
LC_PAPER=sv_SE.UTF-8
LC_NAME=sv_SE.UTF-8
LC_ADDRESS=sv_SE.UTF-8
LC_TELEPHONE=sv_SE.UTF-8
LC_MEASUREMENT=sv_SE.UTF-8
LC_IDENTIFICATION=sv_SE.UTF-8
LC_COLLATE=C.UTF-8
LC_MESSAGES=C.UTF-8
#+end_src

** /etc/vconsole.conf
:PROPERTIES:
:header-args: :tangle "/etc/vconsole.conf" :comments link :mkdirp yes
:ID:       42755c0f-ea0e-4b62-8a8c-5255c0793eea
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Typsnitt och tangentbordslayout*

#+begin_src conf
KEYMAP=sv-latin1
XKBLAYOUT=se
XKBMODEL=pc105
FONT=ter-116b
#+end_src

* Nätverk

** /etc/hosts
:PROPERTIES:
:header-args: :tangle "/etc/hosts" :comments link :mkdirp yes
:ID:       c8c47775-3943-492b-b4c2-b841b1f95c5d
:END:

#+begin_src systemd :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src conf
127.0.0.1   localhost
::1         localhost
127.0.1.1   kurre.stoffepojken.lan  kurre
#+end_src

** /etc/systemd/timesyncd.conf.d/10-fallback_ntp.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/timesyncd.conf.d/10-fallback_ntp.conf" :comments link :mkdirp yes
:ID:       12599962-1417-48ae-afcf-7ed4c0ed2987
:END:

#+begin_src systemd :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Svenska fallback-servar*

#+begin_src systemd
[Time]
FallbackNTP=0.se.pool.ntp.org 1.se.pool.ntp.org 2.se.pool.ntp.org 3.se.pool.ntp.org
#+end_src

#+end_src
** /etc/systemd/resolved.conf.d/10-fallback_dns.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/resolved.conf.d/10-fallback_dns.conf" :comments link :mkdirp yes
:ID:       3d43830d-180e-42d4-ac86-05b62e96d559
:END:

#+begin_src systemd :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Google som fallback-servrar*

#+begin_src systemd
[Resolve]
FallbackDNS=8.8.8.8 8.8.4.4
#+end_src

** /etc/systemd/resolved.conf.d/20-multicast.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/resolved.conf.d/20-multicast.conf" :comments link :mkdirp yes
:ID:       c93ba0f6-3565-428b-8c12-3941de11766a
:END:

#+begin_src systemd :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Mdns*

#+begin_src systemd
[Resolve]
MulticastDNS=no
LLMNR=no
#+end_src

** /etc/systemd/resolved.conf.d/30-disable_cache.conf
:PROPERTIES:
:header-args: :tangle "/etc/systemd/resolved.conf.d/30-disable_cache.conf" :comments link :mkdirp yes
:ID:       4b8127ed-60f1-4ad8-842a-72770bc8fb59
:END:

#+begin_src systemd :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Mdns*

#+begin_src systemd
[Resolve]
Cache=no
#+end_src

** /etc/systemd/network/10-wired.network
:PROPERTIES:
:header-args: :tangle "/etc/systemd/network/10-wired.network" :comments link :mkdirp yes
:ID:       44519c0c-419b-4ba5-b4f9-52b746335f51
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src systemd
[Match]
Name=enp0s31f6

[Link]
RequiredForOnline=routable

[Network]
DHCP=yes
MulticastDNS=no
LLMNR=no
IPv6AcceptRA=yes

[DHCPv4]
UseDomains=yes
RouteMetric=100

[IPv6AcceptRA]
UseDomains=yes
RouteMetric=100
#+end_src

** /etc/systemd/network/20-wireless.network
:PROPERTIES:
:header-args: :tangle "/etc/systemd/network/20-wireless.network" :comments link :mkdirp yes
:ID:       00c4e365-98e5-42dd-9092-78b9e5a76ee0
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src systemd
[Match]
Name=wlp4s0

[Link]
RequiredForOnline=routable

[Network]
DHCP=yes
MulticastDNS=no
LLMNR=no
IPv6AcceptRA=yes

[DHCPv4]
UseDomains=yes
RouteMetric=600

[IPv6AcceptRA]
UseDomains=yes
RouteMetric=600
#+end_src

**  /etc/avahi/avahi-daemon.conf
:PROPERTIES:
:header-args: :tangle "/etc/avahi/avahi-daemon.conf" :comments link :mkdirp yes
:ID:       258aeed0-a752-4344-9df6-9f2f1e79aaf9
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

#+begin_src conf
# This file is part of avahi.
#
# avahi is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# avahi is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with avahi; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.

# See avahi-daemon.conf(5) for more information on this configuration
# file!

[server]
#host-name=foo
#host-name-from-machine-id=no
#domain-name=local
#browse-domains=0pointer.de, zeroconf.org
use-ipv4=yes
use-ipv6=yes
#allow-interfaces=eth0
#deny-interfaces=eth1
#check-response-ttl=no
#use-iff-running=no
#enable-dbus=yes
#disallow-other-stacks=no
#allow-point-to-point=no
#cache-entries-max=4096
#clients-max=4096
#objects-per-client-max=1024
#entries-per-entry-group-max=32
ratelimit-interval-usec=1000000
ratelimit-burst=1000

[wide-area]
#enable-wide-area=no

[publish]
#disable-publishing=no
#disable-user-service-publishing=no
#add-service-cookie=no
#publish-addresses=yes
publish-hinfo=no
publish-workstation=no
#publish-domain=yes
#publish-dns-servers=192.168.50.1, 192.168.50.2
#publish-resolv-conf-dns-servers=yes
#publish-aaaa-on-ipv4=yes
#publish-a-on-ipv6=no

[reflector]
#enable-reflector=no
#reflect-ipv=no
#reflect-filters=_airplay._tcp.local,_raop._tcp.local

[rlimits]
#rlimit-as=
#rlimit-core=0
#rlimit-data=8388608
#rlimit-fsize=0
#rlimit-nofile=768
#rlimit-stack=8388608
#rlimit-nproc=3
#+end_src

**  /etc/nsswitch.conf
:PROPERTIES:
:header-args: :tangle "/etc/nsswitch.conf" :comments link :mkdirp yes
:ID:       220fb9c3-a545-43c1-b45e-0276f377540b
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Mdns för avahi*

#+begin_src conf
# In alphabetical order. Re-order as required to optimize peformance.

aliases:    files
ethers:     files
group:      files [SUCCESS=merge] systemd
gshadow:    files systemd
hosts: 	mymachines mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] files myhostname dns wins
# Allow initgroups to default to the setting for group.
# initgroups: files
netgroup:   files
networks:   files dns
passwd:     files systemd
protocols:  files
publickey:  files
rpc:        files
shadow:     files [UNAVAIL=return] systemd
services:   files
#+end_src

* Bluetooth
**  /etc/bluetooth/main.conf
:PROPERTIES:
:header-args: :tangle "/etc/bluetooth/main.conf" :comments link :mkdirp yes
:ID:       a70a6482-d83c-476e-88ce-30088221c7e8
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Inaktivera timer för visningsläge*
#+begin_src conf
[General]

# Default adapter name
# Defaults to 'BlueZ X.YZ'
Name = Putte

# How long to stay in discoverable mode before going back to non-discoverable
# The value is in seconds. Default is 180, i.e. 3 minutes.
# 0 = disable timer, i.e. stay discoverable forever
DiscoverableTimeout = 0

# Support för BLE
Experimental = true
#+end_src

*Autostarta alla enheter*
#+begin_src conf
[Policy]

# AutoEnable defines option to enable all controllers when they are found.
# This includes adapters present on start as well as adapters that are plugged
# in later on. Defaults to 'true'.
AutoEnable=true
#+end_src

**  /etc/bluetooth/input.conf
:PROPERTIES:
:header-args: :tangle "/etc/bluetooth/input.conf" :comments link :mkdirp yes
:ID:       40e0e9b6-9fed-43cb-8561-40c4de16dbc7
:END:

#+begin_src conf :comments no
##########################
# Tangled with Org-babel #
##########################
#+end_src

*Stäng av auto disconnect av Bluetooth Laser Mouse*

#+begin_src conf
# Configuration file for the input service

# This section contains options which are not specific to any
# particular interface
[General]

# Set idle timeout (in seconds) before the connection will be disconnect and
# the input device is removed.
# Defaults: 0 (disabled)
IdleTimeout=0

#Enable HID protocol handling in userspace input profile
#Defaults to false(hidp handled in hidp kernel module)
UserspaceHID=true
#+end_src
